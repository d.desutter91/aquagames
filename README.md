# AQuAGames

A simple CRUD web app build for automation training purposes.
Simply clone the project, build the solution and start the application.

To run the application without Visual Studio, browse to the folder containing Startup.cs and start command prompt from there. Type 'dotnet run' and the application will start on localhost, the address will be shown in the terminal.

Postman API tests: [![Run in Postman](https://run.pstmn.io/button.svg)](https://app.getpostman.com/run-collection/66d44837edf8361bdf84)