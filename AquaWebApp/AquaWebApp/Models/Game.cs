﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AquaWebApp.Models
{
        public class Game
        {
            public int Id { get; set; }
            [Required]
            public string Title { get; set; }
            [Display(Name = "Year of release")]
            [Range(1900, 2020)]
            public int PublicationYear { get; set; }
            [Display(Name = "Minimum players")]
            [Range(1, 100)]
            public int MinimumPlayers { get; set; }
            [Display(Name = "Maximum players")]
            [Range(1, 100)]
            public int MaximumPlayers { get; set; }
        }
}
