﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using AquaWebApp.Data;
using AquaWebApp.Models;

namespace AquaWebApp
{
    public class IndexModel : PageModel
    {
        private readonly AquaWebApp.Data.AppDbContext _context;

        public IndexModel(AquaWebApp.Data.AppDbContext context)
        {
            _context = context;
        }
        public string CurrentFilter { get; set; }
        public int TotalPages { get; set; }
        [BindProperty(SupportsGet = true)]
        public int CurrentPage { get; set; } = 1;
        public PaginatedList<Game> Game { get;set; }

        public async Task OnGetAsync(string searchString, string currentFilter, int? pageIndex)
        {

            ViewData["currentFilter"] = searchString;
            var games = from s in _context.Game
                           select s;
            if (searchString != null)
            {
                pageIndex = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                games = games.Where(s => s.Title.Contains(searchString));
            }

          
            CurrentFilter = searchString;
            games = games.OrderByDescending(s => s.Id);

            int pageSize = 8;
            TotalPages = (int)Math.Ceiling(decimal.Divide(games.Count(), 8));
            Game = await PaginatedList<Game>.CreateAsync(
                games.AsNoTracking(), pageIndex ?? 1, pageSize);
        }
    }
}
